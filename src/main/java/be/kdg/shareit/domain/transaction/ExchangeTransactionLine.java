package be.kdg.shareit.domain.transaction;

public class ExchangeTransactionLine extends TransactionLine {
    float amount;
    private enum exchangeLineType {BUY, SELL}




    public ExchangeTransactionLine(float amount,long sharepoints) {
        super(sharepoints);
        this.amount = amount;

    }

    public ExchangeTransactionLine( float amount) {
        this.amount = amount;
    }
}
