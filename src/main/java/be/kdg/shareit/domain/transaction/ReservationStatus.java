package be.kdg.shareit.domain.transaction;

import be.kdg.shareit.domain.tool.Tool;
import be.kdg.shareit.util.Period;

import java.time.LocalDateTime;
import java.util.List;

public class ReservationStatus {

    ReservationStatusType type;
    LocalDateTime timestamp;
    List<Byte> confirmationCode;
    boolean isConfirmed;
    private Transaction transaction;

    public ReservationStatus(ReservationStatusType type, boolean isConfirmed) {
        this.type = type;
        this.timestamp = LocalDateTime.now();
        this.isConfirmed = isConfirmed;
    }

    public ReservationStatusType getType() {
        return type;
    }

    public void setReservationStatusType(ReservationStatusType type) {
        this.type = type;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<Byte> getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(List<Byte> confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public void addLine(Tool tool, Period period){
        long amount=0;
        long caution;
        try {

        switch (getType()) {
            case CANCELED_BY_OWNER -> {
                amount=transaction.addLine(-tool.getCancellationAmount(period));
                tool.sendPayment(amount);
            }
            case CANCELED_BY_CONSUMER -> {
                amount=transaction.addLine(tool.getCancellationAmount(period));
                tool.sendPayment(amount);

            }
            case BORROWED -> {
                amount=transaction.addLine(tool.getAmountAndCaution(period));
                tool.sendPayment(amount);
            }
        }
        }
        catch (Exception e){
            this.reset();
        }
    }


    public void reset(){
        type=ReservationStatusType.RESERVED;
    }
    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return "ReservationStatus{" +
                "type=" + type +
                '}';
    }
}
