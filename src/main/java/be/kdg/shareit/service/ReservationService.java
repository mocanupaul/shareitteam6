package be.kdg.shareit.service;

import be.kdg.shareit.domain.transaction.Reservation;
import be.kdg.shareit.domain.transaction.ReservationStatusType;
import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.repository.ReservationRepository;

import java.util.List;

public interface ReservationService {

    public List<Reservation> getReservationsList(String login);

    public ReservationRepository getReservationRepo();

    public void modifyReservation(int reservationId, Transaction transaction, ReservationStatusType reservationStatusType);


}
