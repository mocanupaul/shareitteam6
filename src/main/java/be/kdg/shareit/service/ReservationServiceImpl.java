package be.kdg.shareit.service;

import be.kdg.shareit.domain.transaction.Reservation;
import be.kdg.shareit.domain.transaction.ReservationStatusType;
import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.repository.ReservationRepository;
import be.kdg.shareit.repository.ReservationRepositoryInMemory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReservationServiceImpl implements ReservationService {

    ReservationRepository reservationRepository = new ReservationRepositoryInMemory();


    @Override
    public List<Reservation> getReservationsList(String login) {
        return reservationRepository.getListToDisplayByOwner(login);
//        return reservationList;
    }

    @Override
    public ReservationRepository getReservationRepo() {
        return reservationRepository;
    }


    @Override
    public void modifyReservation(int reservationId, Transaction transaction, ReservationStatusType reservationStatusType) {
        reservationRepository.getReservationById(reservationId).setReservationStatusType(reservationStatusType,transaction);
    }

//    @Override
//    public Reservation retrieveReservation(int reservationId) {
//        Reservation reservation = reservationRepository.getReservationById(reservationId);
//        reservation.setReservationStatusType(ReservationStatusType.BORROWED);
//        return reservation;
//    }
//
//    @Override
//    public Reservation cancelReservation(int reservationId, String canceller) {
//        Reservation reservation = reservationRepository.getReservationById(reservationId);
//        if(reservation.getTool().isOwnedBy(canceller)){
//            reservation.setReservationStatusType(ReservationStatusType.CANCELED_BY_OWNER);
//        }
//        else {
//            reservation.setReservationStatusType(ReservationStatusType.CANCELED_BY_CONSUMER);
//        }
//        return reservation;
//    }
}
