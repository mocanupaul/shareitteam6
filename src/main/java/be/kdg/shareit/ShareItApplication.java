package be.kdg.shareit;

import be.kdg.shareit.domain.tool.Tool;
import be.kdg.shareit.domain.tool.ToolType;
import be.kdg.shareit.domain.transaction.Reservation;
import be.kdg.shareit.domain.transaction.ReservationStatusType;
import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.domain.transaction.TransactionLine;
import be.kdg.shareit.domain.user.User;
import be.kdg.shareit.presentation.Controllers.ReservationController;
import be.kdg.shareit.repository.ReservationRepository;
import be.kdg.shareit.repository.TransactionRepository;
import be.kdg.shareit.service.ReservationService;
import be.kdg.shareit.service.ReservationServiceImpl;
import be.kdg.shareit.service.TransactionService;
import be.kdg.shareit.service.TransactionServiceImpl;
import be.kdg.shareit.util.GeoLocation;
import be.kdg.shareit.util.Period;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.LocalDateTime;

@SpringBootApplication
public class ShareItApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShareItApplication.class, args);
        User Evangelina = new User("evangelina",new GeoLocation(20.20,20.20),1000);
        User Diederich = new User("Diederich",new GeoLocation(20.20,20.20),1000);
        User Kim = new User("Kim",new GeoLocation(20.20,20.20),20);

        ToolType Kitchen = new ToolType(60,"Kitchen");
        ToolType Construction = new ToolType(40,"Construction");
        ToolType Sound = new ToolType(400,"Sound");
        ToolType partyLight= new ToolType(150,"Party Light");

        Tool chainsaw= new Tool(10,"chainsaw",20,20,Construction,Evangelina);
        Tool chainsawBaobab= new Tool(20,"chainsaw",20,20,Construction,Evangelina);
        Tool impactDrill= new Tool(20,"impact drill",20,20,Construction,Evangelina);
        Tool kitchenRobot= new Tool(15,"rbt",20,20,Kitchen,Evangelina);
        Tool paSeismic= new Tool(200,"PA seismic",20,20,Construction,Evangelina);
        Tool blackStrobeMagic= new Tool(50,"Light",20,20,partyLight,Diederich);

        Reservation one=new Reservation(10,new Period(LocalDate.of(2022,12,30),2),2,paSeismic,Evangelina, ReservationStatusType.RESERVED);
        Reservation two=new Reservation(1,new Period(LocalDate.of(2022,12,15),3),3,chainsaw,Diederich,ReservationStatusType.RESERVED);
        Reservation three=new Reservation(2,new Period(LocalDate.of(2022,12,15),4),4,chainsawBaobab,Diederich,ReservationStatusType.RESERVED);
        Reservation four=new Reservation(3,new Period(LocalDate.of(2022,12,17),2),2,kitchenRobot,Diederich,ReservationStatusType.RESERVED);
        Reservation five=new Reservation(4,new Period(LocalDate.of(2022,12,22),4),5,chainsawBaobab,Kim,ReservationStatusType.RESERVED);
        Reservation six=new Reservation(5,new Period(LocalDate.of(2022,12,22),2),2,kitchenRobot,Kim,ReservationStatusType.RESERVED);

        ReservationService reservationService = new ReservationServiceImpl();
        ReservationRepository reservationRepository = reservationService.getReservationRepo();
        reservationRepository.getReservationsList().add(one);
        reservationRepository.getReservationsList().add(two);
        reservationRepository.getReservationsList().add(three);
        reservationRepository.getReservationsList().add(four);
        reservationRepository.getReservationsList().add(five);
        reservationRepository.getReservationsList().add(six);
        TransactionService transactionService = new TransactionServiceImpl();
        ReservationController reservationController = new ReservationController(transactionService, reservationService);

        //scenario 1
        reservationController.printReservationList();

        //scenario 2
//        reservationController.modifyReservation(Diederich,Diederich,1,ReservationStatusType.BORROWED);
//        reservationController.modifyReservation(Diederich,Diederich,2,ReservationStatusType.CANCELED_BY_OWNER);
//        reservationController.printReservationList();

        // scenario 3
//        reservationController.modifyReservation(Diederich,Diederich,1,ReservationStatusType.BORROWED);
//        reservationController.modifyReservation(Diederich,Diederich,2,ReservationStatusType.CANCELED_BY_CONSUMER);


        //scenario 4
        reservationController.modifyReservation(Kim,Kim,4,ReservationStatusType.BORROWED);
        reservationController.modifyReservation(Kim,Kim,5,ReservationStatusType.CANCELED_BY_CONSUMER);
        reservationRepository.getReservationsList().forEach(System.out::println);

        System.out.println(Evangelina.getSharepoints());
        System.out.println(Kim.getSharepoints());
    }

}
