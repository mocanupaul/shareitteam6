package be.kdg.shareit.repository;

import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.domain.user.User;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TransactionRepoInMemory implements TransactionRepository {

    LinkedList<Transaction> transactionList = new LinkedList<>();

    @Override
    public Transaction createTransaction(User payer, User receiver) {
        Transaction transaction = new Transaction(payer, receiver);
        if (transactionList.isEmpty()) {
            transaction.setId(1);
        } else {
            transaction.setId(transactionList.getLast().getId() + 1);
        }
        transactionList.add(transaction);
        return transaction;
    }
}
