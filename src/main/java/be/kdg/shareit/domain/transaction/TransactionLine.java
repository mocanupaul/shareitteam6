package be.kdg.shareit.domain.transaction;

public class TransactionLine {
    long sharepoints = 0;

    public void setSharePoints(long sharepoints){
        this.sharepoints=sharepoints;
    };
    public long getSharePoints(){
        return sharepoints;
    };


    public TransactionLine() {
    }

    public TransactionLine(long sharepoints) {
        this.sharepoints = sharepoints;
    }
}

