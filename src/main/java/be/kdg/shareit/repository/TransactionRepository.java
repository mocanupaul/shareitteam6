package be.kdg.shareit.repository;

import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.domain.user.User;

public interface TransactionRepository {

    Transaction createTransaction(User payer, User Receiver);
}
