package be.kdg.shareit.util;

import java.time.LocalDate;
import java.util.Date;

public final class Period {
    private LocalDate from;
    private LocalDate to;


    public Period(LocalDate from, LocalDate to) {
        this.from = from;
        this.to = to;
    }

    public Period(LocalDate from, int days) {
        this.from = from;
        this.to=from.plusDays(days);
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    public LocalDate getFrom() {
        return from;
    }

    public int getDays(){
        return to.getDayOfYear()-from.getDayOfYear();
    }

    public boolean hasPeriodStarted(){
        return from.isBefore(LocalDate.of(2022,12,15))||
                from.isEqual(LocalDate.of(2022,12,15));
    }

    public LocalDate getTo() {
        return to;
    }
}

