package be.kdg.shareit.domain.user;

import be.kdg.shareit.util.GeoLocation;

import java.util.ArrayList;

public class User {
    private String login;
    private GeoLocation location;
    private long sharepoints;

    public User(String login, GeoLocation location, long sharepoints) {
        this.login = login;
        this.location = location;
        this.sharepoints = sharepoints;
    }


    public void addPayement(long total){
        sharepoints+=total;
    }

    public boolean canPay(long total){
        if(sharepoints>=total){
            sharepoints-=total;
            return true;
        }
        else return false;
    }

    public User(String login, GeoLocation location) {
        this.login = login;
        this.location = location;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation location) {
        this.location = location;
    }

    public long getSharepoints() {
        return sharepoints;
    }

    public void setSharepoints(long sharepoints) {
        this.sharepoints = sharepoints;
    }
}
