package be.kdg.shareit.domain.transaction;

import be.kdg.shareit.domain.user.User;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Transaction {
    int id;
    LocalDateTime timestamp;
    long sharepoints;

    public enum status{UNCONFIRMED,CONFIRMED;}
    public static status status= Transaction.status.UNCONFIRMED;

    private User receiver;
    private User payer;
    private LinkedList<TransactionLine> transactionLines = new LinkedList<>();

    public Transaction(User receiver, User payer) {
        this.receiver = receiver;
        this.payer = payer;
    }

    public Transaction(int id, LocalDateTime timestamp, long sharepoints) {
        this.id = id;
        this.timestamp = timestamp;
        this.sharepoints = sharepoints;
    }

    public Transaction(int id, LocalDateTime timestamp) {
        this.id = id;
        this.timestamp = timestamp;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public User getPayer() {
        return payer;
    }

    public void setPayer(User payer) {
        this.payer = payer;
    }


    public long addLine(long amount) throws Exception {
        transactionLines.add(new TransactionLine(amount));
        long total=0;

        total=transactionLines.getLast().sharepoints;
        if(payer.canPay(total)){
            return total;
        }
        else throw new Exception("Not enough funds");
    }

    public long addLine(long[] amountAndCaution) throws Exception {
            transactionLines.add(new TransactionLine(amountAndCaution[0]));
            transactionLines.add(new TransactionLine(amountAndCaution[1]));

            if(payer.canPay(amountAndCaution[0]+amountAndCaution[1])){
                return amountAndCaution[0]+amountAndCaution[1];
            }
            else throw new Exception("Not enough funds");
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public long getSharepoints() {
        return sharepoints;
    }

    public void setSharepoints(long sharepoints) {
        this.sharepoints = sharepoints;
    }

    public List<TransactionLine> getTransactionLines() {
        return transactionLines;
    }
    public void addTransactionLone(TransactionLine transactionLine){
        this.transactionLines.add(transactionLine);
    }
}
