package be.kdg.shareit.domain.tool;

public class ToolType {
    private long caution;
    private String name;

    public ToolType(long caution, String name) {
        this.caution = caution;
        this.name = name;
    }

    public long getCaution() {
        return caution;
    }

    public void setCaution(long caution) {
        this.caution = caution;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
