package be.kdg.shareit.service;

import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.domain.user.User;

public interface TransactionService {

    Transaction initiateTransaction(User payer, User receiver);
}
