package be.kdg.shareit.service;

import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.domain.user.User;
import be.kdg.shareit.repository.TransactionRepoInMemory;
import be.kdg.shareit.repository.TransactionRepository;

public class TransactionServiceImpl implements TransactionService{
    TransactionRepository transactionRepository =  new TransactionRepoInMemory();


    public TransactionRepository getTransactionRepository(){
        return transactionRepository;
    }

    @Override
    public Transaction initiateTransaction(User payer, User receiver) {
        return transactionRepository.createTransaction(payer,receiver);
    }
}
