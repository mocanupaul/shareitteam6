package be.kdg.shareit.domain.tool;

import be.kdg.shareit.domain.user.User;

import be.kdg.shareit.util.Period;
import java.util.ArrayList;
import java.util.List;

public class Tool {
    private long rentFeePerDay;
    private String description;
    private int purchasePrice;
    private int estimatedValue;
    private ToolType toolType;
    private User owner;

    private List<Tool> subTools = new ArrayList<>();


    public Tool(long rentFeePerDay, String description, int purchasePrice, int estimatedValue, ToolType toolType, User owner) {
        this.rentFeePerDay = rentFeePerDay;
        this.description = description;
        this.purchasePrice = purchasePrice;
        this.estimatedValue = estimatedValue;
        this.toolType = toolType;
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public long getRentFeePerDay() {
        return rentFeePerDay;
    }

    public void setRentFeePerDay(long rentFeePerDay) {
        this.rentFeePerDay = rentFeePerDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(int purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public boolean isOwnedBy(String login){
        return this.owner.getLogin().equals(login);
    }
    public int getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(int estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public ToolType getToolType() {
        return toolType;
    }

    public void setToolType(ToolType toolType) {
        this.toolType = toolType;
    }

    public void addSubTool(Tool tool){
        this.subTools.add(tool);
    }
    public List<Tool> getSubTools(){
        return this.subTools;
    }


    public long getCancellationAmount(Period period){
        // To add method to calculate cancellation amount

        return 8;
    }

    public void sendPayment(long total){
        owner.addPayement(total);
    }
    public long[] getAmountAndCaution(Period period){
        long[] longarr = {0,0};
        longarr[0]=this.rentFeePerDay*period.getDays();
        longarr[1]=this.toolType.getCaution();
        return longarr;
    }
    @Override
    public String toString() {
        return "Tool{" +
                "rentFeePerDay=" + rentFeePerDay +
                ", description='" + description + '\'' +
                ", purchasePrice=" + purchasePrice +
                ", estimatedValue=" + estimatedValue +
                ", toolType=" + toolType +
                ", owner=" + owner +
                ", subTools=" + subTools +
                '}';
    }
}
