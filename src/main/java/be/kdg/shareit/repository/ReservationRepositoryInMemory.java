package be.kdg.shareit.repository;

import be.kdg.shareit.domain.transaction.Reservation;

import java.util.ArrayList;
import java.util.List;

public class ReservationRepositoryInMemory implements ReservationRepository{

    List<Reservation> reservationList = new ArrayList<>();

    @Override
    public List<Reservation> getReservationsList() {
//        reservationList.forEach(System.out::println);
        return reservationList;
    }

    @Override
    public List<Reservation> getListToDisplayByOwner(String owner) {
        return reservationList.stream().filter(reservation -> reservation.canDisplay(owner)).toList();
    }

    @Override
    public Reservation getReservationById(int id) {
        return reservationList.stream().filter(reservation -> reservation.getId()==id).toList().get(0);
    }
}
