package be.kdg.shareit.presentation.Controllers;

import be.kdg.shareit.domain.transaction.ReservationStatusType;
import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.domain.user.User;
import be.kdg.shareit.service.ReservationService;
import be.kdg.shareit.service.ReservationServiceImpl;
import be.kdg.shareit.service.TransactionService;
import be.kdg.shareit.service.TransactionServiceImpl;
import org.springframework.stereotype.Controller;

//@Controller
public class ReservationController {

    TransactionService transactionService = new TransactionServiceImpl();
    ReservationService reservationService = new ReservationServiceImpl();

    public void printReservationList() {
        reservationService.getReservationsList("evangelina").forEach(System.out::println);
    }

    public ReservationController(TransactionService transactionService, ReservationService reservationService) {
        this.transactionService = transactionService;
        this.reservationService = reservationService;
    }

    public void modifyReservation(User receiver, User payer, int reservationId, ReservationStatusType reservationStatusType){
        Transaction transaction =transactionService.initiateTransaction(payer,receiver);
        reservationService.modifyReservation(reservationId,transaction,reservationStatusType);
    }

}
