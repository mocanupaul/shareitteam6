package be.kdg.shareit.domain.transaction;

public class ReservationTransactionLine extends  TransactionLine{
    ReservationStatusType statusType;
    public enum reservationTransactionType{RENT,CAUTION,CANCELLATION};

    private Reservation reservation;

    public ReservationTransactionLine(ReservationStatusType statusType) {
        this.statusType = statusType;
    }

    public ReservationTransactionLine(long sharepoints, ReservationStatusType statusType) {
        super(sharepoints);
        this.statusType = statusType;
    }

    public ReservationStatusType getStatusType() {
        return statusType;
    }

    public void setStatusType(ReservationStatusType statusType) {
        this.statusType = statusType;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }
}
