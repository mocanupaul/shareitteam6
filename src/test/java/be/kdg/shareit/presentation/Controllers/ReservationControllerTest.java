package be.kdg.shareit.presentation.Controllers;

import be.kdg.shareit.domain.tool.Tool;
import be.kdg.shareit.domain.tool.ToolType;
import be.kdg.shareit.domain.transaction.Reservation;
import be.kdg.shareit.domain.transaction.ReservationStatusType;
import be.kdg.shareit.domain.transaction.Transaction;
import be.kdg.shareit.domain.user.User;
import be.kdg.shareit.repository.ReservationRepository;
import be.kdg.shareit.service.ReservationService;
import be.kdg.shareit.service.ReservationServiceImpl;
import be.kdg.shareit.service.TransactionService;
import be.kdg.shareit.service.TransactionServiceImpl;
import be.kdg.shareit.util.GeoLocation;
import be.kdg.shareit.util.Period;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReservationControllerTest {

    private ReservationService reservationService;
    ReservationRepository reservationRepository ;
    TransactionService transactionService;
    User Evangelina = new User("evangelina",new GeoLocation(20.20,20.20),1000);
    User Diederich = new User("Diederich",new GeoLocation(20.20,20.20),1000);
    User Kim = new User("Kim",new GeoLocation(20.20,20.20),20);
    @BeforeEach
    public void setup(){
        reservationService=new ReservationServiceImpl();
        reservationRepository=reservationService.getReservationRepo();
        transactionService= new TransactionServiceImpl();



        ToolType Kitchen = new ToolType(60,"Kitchen");
        ToolType Construction = new ToolType(40,"Construction");
        ToolType Sound = new ToolType(400,"Sound");
        ToolType partyLight= new ToolType(150,"Party Light");

        Tool chainsaw= new Tool(10,"chainsaw",20,20,Construction,Evangelina);
        Tool chainsawBaobab= new Tool(20,"chainsaw",20,20,Construction,Evangelina);
        Tool impactDrill= new Tool(20,"impact drill",20,20,Construction,Evangelina);
        Tool kitchenRobot= new Tool(15,"rbt",20,20,Kitchen,Evangelina);
        Tool paSeismic= new Tool(200,"PA seismic",20,20,Construction,Evangelina);
        Tool blackStrobeMagic= new Tool(50,"Light",20,20,partyLight,Diederich);

        Reservation one=new Reservation(10,new Period(LocalDate.of(2022,12,30),2),2,paSeismic,Evangelina, ReservationStatusType.RESERVED);
        Reservation two=new Reservation(1,new Period(LocalDate.of(2022,12,15),3),3,chainsaw,Diederich,ReservationStatusType.RESERVED);
        Reservation three=new Reservation(2,new Period(LocalDate.of(2022,12,15),4),4,chainsawBaobab,Diederich,ReservationStatusType.RESERVED);
        Reservation four=new Reservation(3,new Period(LocalDate.of(2022,12,17),2),2,kitchenRobot,Diederich,ReservationStatusType.RESERVED);
        Reservation five=new Reservation(4,new Period(LocalDate.of(2022,12,22),4),5,chainsawBaobab,Kim,ReservationStatusType.RESERVED);
        Reservation six=new Reservation(5,new Period(LocalDate.of(2022,12,22),2),2,kitchenRobot,Kim,ReservationStatusType.RESERVED);
        reservationRepository.getReservationsList().add(one);
        reservationRepository.getReservationsList().add(two);
        reservationRepository.getReservationsList().add(three);
        reservationRepository.getReservationsList().add(four);
        reservationRepository.getReservationsList().add(five);
        reservationRepository.getReservationsList().add(six);

    }
    @Test
    void printReservationList() {
        reservationService.getReservationsList("evangelina").forEach(System.out::println);
        assertTrue((long) reservationService.getReservationsList("evangelina").size()==2);
    }

    @Test
    void modifyReservation() {
        //scenario 2
        Transaction transaction =transactionService.initiateTransaction(Diederich,Diederich);
        reservationService.modifyReservation(1,transaction,ReservationStatusType.BORROWED);
        reservationService.modifyReservation(2,transaction,ReservationStatusType.CANCELED_BY_OWNER);
        boolean scen2=Diederich.getSharepoints()==938&&Evangelina.getSharepoints()==1062;
        assertTrue(scen2);

        //scenario 3
        Transaction transaction1 =transactionService.initiateTransaction(Diederich,Diederich);
        reservationService.modifyReservation(1,transaction1,ReservationStatusType.BORROWED);
        reservationService.modifyReservation(2,transaction1,ReservationStatusType.CANCELED_BY_OWNER);
        boolean scen3=Diederich.getSharepoints()==876&&Evangelina.getSharepoints()==1124;
        assertTrue(scen3);

        //scenario 4
        Transaction transaction2 =transactionService.initiateTransaction(Kim,Kim);
        reservationService.modifyReservation(4,transaction2,ReservationStatusType.BORROWED);
        reservationService.modifyReservation(5,transaction2,ReservationStatusType.CANCELED_BY_CONSUMER);
        boolean scen4=Kim.getSharepoints()==12&&Evangelina.getSharepoints()==1132;

        assertTrue(scen4);
    }
}