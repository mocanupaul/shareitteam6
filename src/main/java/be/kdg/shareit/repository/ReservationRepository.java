package be.kdg.shareit.repository;

import be.kdg.shareit.domain.transaction.Reservation;

import java.util.List;

public interface ReservationRepository {

    public List<Reservation> getReservationsList();
    List<Reservation> getListToDisplayByOwner(String owner);

    Reservation getReservationById(int id);
}
