package be.kdg.shareit.domain.transaction;

import be.kdg.shareit.domain.tool.Tool;
import be.kdg.shareit.domain.user.User;
import be.kdg.shareit.util.Period;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Reservation {
    int id;
    private Period period;
    private Tool tool;
    private User borrower;
    private LinkedList<ReservationStatus> history= new LinkedList<>();

    private int days;
//    private ReservationStatusType reservationStatusType;



    public Reservation(int id, Period period,int days, Tool tool, User borrower, ReservationStatusType reservationStatusType) {
        this.id = id;
        this.period = period;
        this.tool = tool;
        this.days=days;
        this.borrower = borrower;
    }


    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setReservationStatusType(ReservationStatusType reservationStatusType) {
        this.history.getLast().setReservationStatusType(reservationStatusType);
    }
    public void setReservationStatusType(ReservationStatusType reservationStatusType, Transaction transaction) {
        this.history.add(new ReservationStatus(reservationStatusType,true));
        this.history.getLast().setTransaction(transaction);
        this.history.getLast().addLine(tool,period);
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Tool getTool() {
        return tool;
    }

    public boolean canDisplay(String login){
        return tool.isOwnedBy(login)&& period.hasPeriodStarted();
    }

    public void setTool(Tool tool) {
        this.tool = tool;
    }



    public User getBorrower() {
        return borrower;
    }

    public void setBorrower(User borrower) {
        this.borrower = borrower;
    }

    public List<ReservationStatus> getHistory() {
        return history;
    }

    public void setHistory(LinkedList<ReservationStatus> history) {
        this.history = history;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", period=" + period +
                ", tool=" + tool +
                ", borrower=" + borrower +
                ", history=" + history +
                '}';
    }
}
