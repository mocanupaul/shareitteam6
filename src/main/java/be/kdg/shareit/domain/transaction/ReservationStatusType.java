package be.kdg.shareit.domain.transaction;

public enum ReservationStatusType {
    NEW, RESERVED, BORROWED, RETURNED, CANCELED_BY_OWNER, CANCELED_BY_CONSUMER, RESERVATION_REFUSED
}
